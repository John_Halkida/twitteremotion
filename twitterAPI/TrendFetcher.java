package twitterAPI;

import java.util.ArrayList;
import java.util.Date;

import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Fetches a list of Trendword objects and pushes them to Mongo
 * @author alkoclick
 *
 */
public class TrendFetcher implements Runnable{
	ConfigurationBuilder cb;
	Twitter twitter;
	private MongoHandler mongo;
	
	public TrendFetcher(Twitter twitter,MongoHandler mongoHandler) {
		super();
		this.twitter = twitter;
		this.mongo = mongoHandler;
	}

	@Override
	public void run() {
		try {
			//System.out.println(Messages.getString("TrendFetcher.4"));
			Trends trends = twitter.getPlaceTrends(1); // Get global trends
			
			//System.out.println(Messages.getString("TrendFetcher.5"));
			ArrayList<TrendWord> newTrends = new ArrayList<>();
			Date asOfDate = trends.getAsOf();
			Date dateForMONGO = mongo.getDateInEET(asOfDate);
			
			System.out.println(Messages.getString("TrendFetcher.2")); //$NON-NLS-1$
			for (Trend trend : trends.getTrends()){
				
				// If this trend contains no english characters, then ignore it
				if ( trend.getName().replaceAll("[a-z]+", "").replace("[A-Z]+", "").length()==trend.getName().length() ) continue;
				
				String trendName = trend.getName().replace("#", "");  // Removing hashtags - do we need them?
				System.out.println(trendName);
				TrendWord trendWord = new TrendWord(trendName,dateForMONGO);
				newTrends.add(trendWord);
			}
			Looper.updateTrends(newTrends,dateForMONGO);

			Stream stream = new Stream(new Authentication().authenticateStream(), Looper.getActiveTrends(),mongo);
	        Thread StreamThread = new Thread(stream);
	        StreamThread.start();
			
			System.out.println(Messages.getString(Messages.getString("TrendFetcher.11"))+trends.getAsOf()); //$NON-NLS-1$
			System.out.println(Messages.getString(Messages.getString("TrendFetcher.12"))+trends.getTrendAt()); //$NON-NLS-1$
			
		} catch (TwitterException e) {
			System.out.println(Messages.getString("TrendFetcher.13")); //$NON-NLS-1$
			e.printStackTrace();
		}
	}
}
