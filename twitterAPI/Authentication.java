package twitterAPI;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Handles authentication for all twitter and twitterStream objects
 */
public class Authentication {
	
	/**
	 * Creates and returs a new Twitter object using the hardcoded authentication information
	 * @return Twitter object
	 */
	public Twitter authenticate(){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setJSONStoreEnabled(true)
		  .setOAuthConsumerKey(Messages.getString("Authentication.0")) //$NON-NLS-1$
		  .setOAuthConsumerSecret(Messages.getString("Authentication.1")) //$NON-NLS-1$
		  .setOAuthAccessToken(Messages.getString("Authentication.2")) //$NON-NLS-1$
		  .setOAuthAccessTokenSecret(Messages.getString("Authentication.3")); //$NON-NLS-1$
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		return twitter;
	}
	
	/**
	 * Creates and returs a new TwitterStream object using the hardcoded authentication information
	 * @return TwitterStream object
	 */
	public TwitterStream authenticateStream(){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setJSONStoreEnabled(true)
		  .setOAuthConsumerKey(Messages.getString("Authentication.0")) //$NON-NLS-1$
		  .setOAuthConsumerSecret(Messages.getString("Authentication.1")) //$NON-NLS-1$
		  .setOAuthAccessToken(Messages.getString("Authentication.2")) //$NON-NLS-1$
		  .setOAuthAccessTokenSecret(Messages.getString("Authentication.3")); //$NON-NLS-1$
		TwitterStreamFactory tf = new TwitterStreamFactory(cb.build());
		TwitterStream twitterStream = tf.getInstance();
		return twitterStream;
	}
}
