package twitterAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TimeZone;

import com.mongodb.MongoClient;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;

import twitter4j.JSONObject;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.Block;
import static com.mongodb.client.model.Filters.*;

public class MongoHandler {
	private MongoDatabase db;
	private MongoClient mongoClient;
	
	private HashSet<String> users;
	private ArrayList<String> trends; 
	
	/**
	 * Creates a mongoClient object and connects to the local twitter db
	 */
	public MongoHandler(){
		
		mongoClient = new MongoClient();
		db = mongoClient.getDatabase("twitter3");
		
		users = new HashSet<String>();
		trends  = new ArrayList<String>();
	}
	
	public class TweetObject{
		private String text;
		private ObjectId id;
		
		public TweetObject(ObjectId id1,String text1){
			text = text1;
			id = id1;
		}
		
		public String text(){return text;}
		public ObjectId id(){return id;}
	}
	
	/**
	 * Returns the date (DATE FOR MONGO) and time of param "myDate" in EET Timezone as a "java.util.Date" object.
	 * */
	public Date getDateInEET(Date myDate){
		
		String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
	    sdf.setTimeZone(TimeZone.getTimeZone("EET"));
	    String utcTime = sdf.format(myDate);
	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
	    Date dateToReturn=null;
	    
	    try
	    {
	        dateToReturn = (Date)dateFormat.parse(utcTime);
	    }
	    catch (ParseException e)
	    {
	    	System.out.println("Date parsing Exception.");
	        e.printStackTrace();
	    }
	    return dateToReturn;

	}
	
	/**
	 * Adds a given tweetJson object to the given collection
	 * @param tweetJSON The tweet to be parsed to string and added
	 * @param collection The collection to put the tweet into
	 */
	public void addTweet(JSONObject tweetJSON,String collection){
		
			db.getCollection(collection).insertOne(Document.parse(tweetJSON.toString()));
//			System.out.println("Tweet inserted to MongoDB");
		
	}
	
	/* If a trend with a "content" field equal to the content of the trend parameter exists,
	 * then push a new entry in it's "datesList" list and update it's "timeLastSeen" field.
	 * If such a trend does NOT exist then create one such document and perform the above operations.
	 * When this is done, if a new document was created during the previous operations then set it's "timeFirstSeen" field to a date, 
	 * if not then keep it's "timeFirstSeen" field untouched. */
	public void isNewOrRecurringOperation(TrendWord trend){
		
		Date now = new Date();
	    Date dateUTC = getDateInEET(now);//Mongo thinks that whatever Date we add is Local time and converts it from Local to UTC. So we add Local time in Greece (and it stores UTC that is 2hours behind)
				
		UpdateOptions uo = new UpdateOptions();
		uo.upsert(true);
		
		UpdateResult res = db.getCollection("trendWords").updateOne(
								
				new Document("content", trend.getContent())
				,
				new Document("$push",
					new Document("datesList",
							new Document("start",dateUTC)
							.append("end", dateUTC)
					)
				)
				.append("$set", new Document("timeLastSeen",dateUTC)),
				uo
		);
		
		//It was a NEW trend!
		if(res.getUpsertedId() != null){
			
			ArrayList<Document> datesList = new ArrayList<Document>();
			datesList.add(new Document("start",dateUTC)
								.append("end", dateUTC));
			
			db.getCollection("trendWords").findOneAndUpdate(
					
					new Document("content", trend.getContent()),
					new Document("$set",
							new Document("timeFirstSeen",dateUTC)
							.append("datesList", datesList)
					)
			);
		}
	}
	
	/* Finds a document of "trendWords" with "content"="DUMMY" and updates the latest "end" Date field inside it's "myList" list field */
	public void isActiveOperation(TrendWord trend){
		
		int activeHours = 2;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.roll(Calendar.HOUR_OF_DAY, -activeHours);
		
		Date now = new Date();
		Date past = cal.getTime();
		
		Date pastUTC = getDateInEET(past);
		Date nowUTC  = getDateInEET(now);
		
		db.getCollection("trendWords").findOneAndUpdate(
			
				new Document("content", trend.getContent())
					.append("datesList.end",
							new Document("$gte",pastUTC)
				),
				new Document("$set",
						new Document("datesList.$.end",nowUTC)
						.append("timeLastSeen", nowUTC)
				)
		);
		
	}
	
	public void addTrendWord(TrendWord trend, boolean isActive){

		if(isActive){
			isActiveOperation(trend);
		}else{
			isNewOrRecurringOperation(trend);
		}
	}
	
	/**
	 * Create and add a dummy Trend object on a custom date
	 * @param content The content of the trendWord object
	 * @param dateFirstSeen
	 * @param dateLastSeen
	 */
	public void addDummy(String content, Date dateFirstSeen, Date dateLastSeen){
		db.getCollection("trendWords").insertOne(
				new Document("content",content)
				.append("timeFirstSeen", dateFirstSeen)
				.append("timeLastSeen", dateLastSeen)
				.append("myList", new ArrayList<Date>())
				);
	}
	

	public boolean exists(String collection,ObjectId id){
		return db.getCollection(collection).find(eq("_id", id)).iterator().hasNext();
	}
	
	/**
	 * Shut down client gracefully
	 */
	public void closeClient() {
		mongoClient.close();
	}
	
	/**
	 * Fills "users":contains the screenNames of all users
	 * and  "trends":contains the content of all trends.
	 * */
	public void fetchUsersAndTrends(){
		
		FindIterable<Document> iterableTrends = db.getCollection("trendWords").find().projection(new Document("content",1));
		MongoCursor<Document> cursor = iterableTrends.iterator();
		while(cursor.hasNext()){
			String content = cursor.next().getString("content");
			trends.add(content);
			
			DistinctIterable<String> iterableTweets = db.getCollection(content).distinct("user.screenName",String.class);
			MongoCursor<String> cursor2 = iterableTweets.iterator();
			while(cursor2.hasNext()){
				String screenName = cursor2.next();
				users.add(screenName);
			}
		}
	}
	
	public HashSet<String> getUsers(){return users;}
	public ArrayList<String> getTrends(){return trends;}
	
	/**
	 * Returns the text of all tweets posted by a specific user. 
	 * */
	public ArrayList<TweetObject> getTweets(String user){
		
		ArrayList<TweetObject> tweets = new ArrayList<TweetObject>();
		
		Iterator<String> it = trends.iterator();
		while(it.hasNext()){
			String trend = it.next();
			
			FindIterable<Document> tweetsIterable = db.getCollection(trend).find(eq("user.screenName", user)).projection(new Document("text",1).append("_id", 1));
			MongoCursor<Document> cursor = tweetsIterable.iterator();
			while(cursor.hasNext()){
				
				Document doc = cursor.next();
				String tweet = doc.getString("text");
				ObjectId id = doc.getObjectId("_id");
				
				//System.out.println("id:"+id+" ->  "+tweet + " was posted on "+trend +" (id exists:"+exists(trend, id)+")");
				
				tweets.add(new TweetObject(id,tweet));
			}
		}
		
		return tweets;
	}
	
	
	public void deleteTweet(ObjectId id){
		
	}
	
	/**
	 * Prints a given collection
	 * @param collection
	 */
	public void printTrendWords(String trend) {
		FindIterable<Document> iterable = db.getCollection("trendWords").find(eq("content", trend));
		iterable.forEach(new Block<Document>() {
		    @Override
		    public void apply(final Document document) {
				try {
					//System.out.println(new JSONObject(document.toJson()));
					System.out.println("content:"+document.getString("content") +
							"\ntimeFirstSeen:"+document.getDate("timeFirstSeen") +
							"\ntimeLastSeen:"+document.getDate("timeLastSeen")+
							"\ndatesList: [");
					
					@SuppressWarnings("unchecked")
					ArrayList<Document> dates = (ArrayList<Document>) document.get("datesList");
					Iterator<Document> it = dates.iterator();
					while(it.hasNext()){
						
						Document doc = it.next();
						Date start = doc.getDate("start");
						Date end = doc.getDate("end");
						
						System.out.println("\t"+"start:"+start+" , end:"+end);
					}
					System.out.println("]");
				//} catch (JSONException e) {
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		});
	}
}
