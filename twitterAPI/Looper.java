package twitterAPI;

import java.util.ArrayList;
import java.util.Date;

import twitter4j.Twitter;

/**
 * A simple thread which starts new threads every 5 mins
 */
public class Looper implements Runnable{
	private boolean isRunning;
	private Twitter twitter;
	private static ArrayList<TrendWord> activeTrends;
	private static final long sleepTime = 300000; // TODO Turn this to 300000 for the final version
	private static MongoHandler mongo;
	
	public Looper(Twitter twitter,boolean isRunning) {
		initialize(twitter, isRunning);
		mongo = new MongoHandler();
	}
	
	private void initialize(Twitter twitter,boolean isRunning){
		activeTrends = new ArrayList<>();
		this.isRunning = isRunning;
		this.twitter = twitter;
	}
	
	@Override
	public void run() {
		
		while (isRunning){
			try {
				Thread trendThread = new Thread(new TrendFetcher(twitter,mongo));
				trendThread.start();

				System.out.println(Messages.getString("Looper.1")); //$NON-NLS-1$
				//Thread.sleep(sleepTime);
				int minutesUntilNextFetch = 1;
				Thread.sleep(1000*60*minutesUntilNextFetch);
		        Stream.kill();
			} catch (InterruptedException e) {
				System.out.println(Messages.getString(Messages.getString("Looper.2"))); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
		//Looper has stopped running
		mongo.closeClient();
	}
	
	/**
	 * Updates all activeTrends by removing stale entries
	 * and updating entries that exist in newTrends
	 * @param newTrends The TrendWords fetched most recently
	 * @param date 
	 */
	public static void updateTrends(ArrayList<TrendWord> newTrends, Date dateMONGO){
		
		for (TrendWord trendWord : activeTrends){
			if (trendWord.hasExpired(dateMONGO) && !isIn(trendWord,newTrends)){ // Removed trends
				activeTrends.remove(trendWord);
			}
			else {
				trendWord.updateDate(dateMONGO); // Updated Trends
				if (isIn(trendWord, newTrends)) {
					removeTrends(trendWord,newTrends);
					mongo.addTrendWord(trendWord, true);
				}
			}
		}
		for (TrendWord newTrendWord : newTrends){ // New Trends
			activeTrends.add(newTrendWord);
			mongo.addTrendWord(newTrendWord, false);
		}
	}
	
	/**
	 * Removes a trendWord from the newTrends if their contents match
	 * @param trendWord
	 * @param newTrends
	 */
	private static void removeTrends(TrendWord trendWord, ArrayList<TrendWord> newTrends) {
		int index = -1;
		for (int i=0;i<newTrends.size();++i){
			if (newTrends.get(i).getContent().equals(trendWord.getContent())) index=i;
		}
		if (index!=-1) newTrends.remove(index);
	}

	/**
	 * Checks if there is a TrendWord object in the array with content
	 * matching that of the given TrendWord, toBeFound
	 * @param toBeFound The TrendWord whose content we are matching against the ArrayList objects
	 * @param newTrends
	 * @return True if there exists a TrendWord in newTrends with content equal to toBeFound.getContent()
	 */
	private static boolean isIn(TrendWord toBeFound, ArrayList<TrendWord> newTrends) {
		for (TrendWord trendWord : newTrends){
			if (trendWord.getContent().equals(toBeFound.getContent())) return true;
		}
		return false;
	}

	/**
	 * Pretty useless, can use Thread.isAlive() instead. The only difference is while the thread
	 * is sleeping after it has been sent a setRunning->false. isAlive will then return true, while
	 * this will return false
	 * @return Thread isAlive status. 
	 */
	public boolean isRunning() {
		return isRunning;
	}
	
	/**
	 * Controls when the loop stops
	 * @param isRunning Set false to stop the looper on the next iteration
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public static ArrayList<TrendWord> getActiveTrends() {
		return activeTrends;
	}
	
}
