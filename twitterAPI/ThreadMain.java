package twitterAPI;

import twitter4j.Twitter;


/**
 * Main thread handling class
 */
public class ThreadMain implements Runnable {
	
	@Override
	public void run() {
		
//		TODO (Alex) Toggle this on for final release and set it to 3 days duration
		int minutesToRunFor = 60*24*3;
		
		System.out.println(Messages.getString("ThreadMain.3")); //$NON-NLS-1$
		Twitter twitter = new Authentication().authenticate(); // Authenticate the twitter Object
		
		Looper looper = new Looper(twitter, true); // Use it for a new looperThread
		Thread looperThread = new Thread(looper);
		looperThread.start();
		
		System.out.println(Messages.getString("ThreadMain.4")); //$NON-NLS-1$
		try {
		Thread.sleep(1000*60*minutesToRunFor);  // TODO Set this to 1000*3600*24*3
		} catch (InterruptedException e) {
			System.out.println(Messages.getString("ThreadMain.5")); //$NON-NLS-1$
			e.printStackTrace();
		}
		finally {
			looper.setRunning(false);
		}
		System.out.println(Messages.getString("ThreadMain.6")); //$NON-NLS-1$
        
	}

}
