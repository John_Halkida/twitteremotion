package twitterAPI;

import java.util.ArrayList;

import twitter4j.FilterQuery;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.Query;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;

public class Stream implements Runnable{
	static TwitterStream twitterStream;
	String [] queries;
	private static MongoHandler mongo;

	/**
	 * @param twitterStream
	 */
	public Stream(TwitterStream twitterStream, MongoHandler mongoHandler) {
		super();
		this.twitterStream = twitterStream;
		mongo = mongoHandler;
	}
	
	/**
	 * Alternative constructor including a query string
	 * @param twitterStream
	 */
	public Stream(TwitterStream twitterStream,ArrayList<TrendWord> queries, MongoHandler mongoHandler) {
		super();
		this.twitterStream = twitterStream;
		this.queries = new String[queries.size()];
		mongo = mongoHandler;
		int count = 0;
		for (TrendWord trendWord : queries){
			this.queries[count++] = trendWord.getContent();
		}
	}

	@Override
	public void run() {
		
		StatusListener listener = buildListener();
	    twitterStream.addListener(listener);
	    
	    if (queries!=null){
	    	FilterQuery fQ = new FilterQuery();
	    	
	    	fQ.track(queries);
	    	
	    	twitterStream.filter(fQ);
	    }
	    else {
	    	twitterStream.sample("en");
	    }
	}

	/**
	 * Stops the stream
	 */
	public static void kill(){
		twitterStream.cleanUp();
//		mongo.closeClient();

	}
	
	/**
	 * Creates a StatusListener for usage in streams
	 * @return StatusListener
	 */
	private StatusListener buildListener() {
			
			return new StatusListener() {
	    	
	        @Override
	        public synchronized void onStatus(Status status) {
	            JSONObject statusObject = new JSONObject( status );
	            String text = "";
	            try {
					text = statusObject.get("text").toString();
				} catch (JSONException e) {
					e.printStackTrace();
				}
	            for ( String query : queries){
	            	if (text.contains(query)) 
	            		mongo.addTweet(statusObject, query);
	            }
	        }

	        @Override
	        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
	            System.out.println(Messages.getString("Stream.9") + numberOfLimitedStatuses); //$NON-NLS-1$
	        }

	        @Override
	        public void onScrubGeo(long userId, long upToStatusId) {
	            System.out.println(Messages.getString("Stream.10") + userId + Messages.getString("Stream.11") + upToStatusId); //$NON-NLS-1$ //$NON-NLS-2$
	        }

	        @Override
	        public void onStallWarning(StallWarning warning) {
	            System.out.println(Messages.getString("Stream.12") + warning); //$NON-NLS-1$
	        }

	        @Override
	        public void onException(Exception ex) {
	            ex.printStackTrace();
	        }

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				System.out.println(Messages.getString("Stream.6")); //$NON-NLS-1$
			}
	    };
	}
	
}
