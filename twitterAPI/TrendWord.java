package twitterAPI;

import java.util.Date;

/**
 * Class representing a trending word object
 */
public class TrendWord {
	private Date dateUpdated;
	private Date dateFirstSeen;
	private String content;
//	private DateFormat dateFormat;
	private static final long keepAliveTime = 1000 * 7200;
	
	/**
	 * @param content The trending word
	 * @param asOfDate 
	 */
	public TrendWord(String content, Date asOfDate) {
		this.content = content;
		dateFirstSeen = asOfDate;
		updateDate(asOfDate);
	}
	
	/**
	 * Overloaded constructor in case Date is not provided
	 * @param content The trending word
	 */
/*
	public TrendWord(String content) {
		this.content = content;
//		dateFormat = new SimpleDateFormat(Messages.getString("TrendWord.dateFormat"), Locale.ENGLISH);
		updateDate(new Date());
	}
*/

	public String getContent(){
		return content;
	}

	public Date getDateFirstSeen() {
		return dateFirstSeen;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public void updateDate(Date asOfDate){
		dateUpdated = asOfDate;
	}

	//public boolean isActive(Date date){
	//	return !hasExpired(date);
	//}

	public boolean hasExpired(Date asOfDate) {
		return (asOfDate.getTime() - dateUpdated.getTime() > keepAliveTime ? true : false);
	}
	
}
