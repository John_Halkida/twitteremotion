package twitterAPI;

import java.util.ArrayList;
import java.util.Iterator;

import org.bson.Document;

import com.mongodb.MongoClient;

import SentimentAnalysis.*;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitterAPI.MongoHandler.TweetObject;

public class Main {
	
	public static void main(String[] args) {		
		
//		MongoClient mongoClient = new MongoClient();
//		mongoClient.dropDatabase("twitte3");
//		mongoClient.close();	
//
//		new Thread(new ThreadMain()).start();
		
		//Example of how to use sentiment extractor
		//********************************************
		//String testTweet1 = "I am too Disappointed, furious and sad at the same time";
		//String testTweet2 = "I feel really happy and joyful today";
		//SentimentInterface test = new SentimentInterface();
		//double[] scores1 = test.extractSentiments(testTweet1);// scores1[0] contains score for anger, scores1[1] contains score for disgust, scores1[2] contains score for fear, scores1[3] contains score for joy, scores1[4] contains score for sadness, scores1[5] contains score for surprise. 
		//double[] scores2 = test.extractSentiments(testTweet2);// the same as above.
		//**********************************************
		
//		Hint: To remove comments from a line (or a block), use Ctrl+Shift+C! (Eclipse)

		

//Drop twitter2 --------------------------------------------------------------------------------------------------------
		
		MongoClient mongoClient = new MongoClient();
		mongoClient.dropDatabase("twitter3");
		mongoClient.close();	
		
//Create the collections----------------------------------------------------------------------------------
		
		MongoHandler mongo = new MongoHandler();
		
		mongo.addTrendWord(new TrendWord("trend1",null),false);
		mongo.addTrendWord(new TrendWord("trend2",null),false);
		mongo.addTrendWord(new TrendWord("trend3",null),false);
		mongo.addTrendWord(new TrendWord("trend4",null),false);
		mongo.addTrendWord(new TrendWord("trend5",null),false);
	
//Add some tweets fot user1 ------------------------------------------------------------------------------
		
		String user = "Dave Brubeck";
		for(int i=0;i<10;i++){
		
			JSONObject tweet = new JSONObject();
			try {
				tweet.put("text", "i'm a pianist "+i);
				tweet.put("user", new JSONObject().put("screenName",user).put("country", "Norway"));
			} catch (JSONException e){}
			
			mongo.addTweet(tweet, "trend"+((i%5)+1));
		}
		
//Add some tweets for user2 -----------------------------------------------------------------------------
		
		user = "Paul Desmond";
		for(int i=0;i<10;i++){
		
			JSONObject tweet = new JSONObject();
			try {
				tweet.put("text", "i'm a saxophonist "+i);
				tweet.put("user", new JSONObject().put("screenName",user).put("country", "America"));
			} catch (JSONException e){}
			
			mongo.addTweet(tweet, "trend"+((i%5)+1));
		}
		
//Get tweets for each user ------------------------------------------------------------------------------
		
		mongo.fetchUsersAndTrends();
		
		Iterator<String> users = mongo.getUsers().iterator();
		while(users.hasNext()){
			String u = users.next();
			
			ArrayList<TweetObject> tweets = mongo.getTweets(u);
			
			//Print the tweets
			Iterator<TweetObject> it = tweets.iterator();
			while(it.hasNext()){
				TweetObject tweet = it.next();
				System.out.println("id:"+tweet.id()+" , text:"+tweet.text());
			}
		}
		
		
	}

}
