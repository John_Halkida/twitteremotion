package SentimentAnalysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LineSplitter{
	
	private ArrayList<String> lines;
	
	public LineSplitter()
	{
		lines = new ArrayList<>();
	}
	/*This function returns an arraylist that contains all the lines of a document*/
	public ArrayList<String> splitToLines(String path,String fileName) 
	{
		File file = new File(path+fileName);
	    FileReader reader = null;
		try {
			reader = new FileReader(file);
			BufferedReader buffReader = new BufferedReader(reader);
		    try {
		        String line;
		        while((line = buffReader.readLine()) != null){
			         lines.add(line.toLowerCase());
		        }
		    }
			   catch(IOException e){
			       e.printStackTrace();
			   }
		    finally{
		    	try {
					buffReader.close();
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		} 
		catch (FileNotFoundException e1) 
		{
			System.out.println("WARNING: The file "+path+fileName+" does not exist. Maybe there are hidden files in the folder.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return lines;
	}
}

