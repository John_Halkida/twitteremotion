package SentimentAnalysis;

import java.util.ArrayList;
import java.util.Hashtable;

public class SentimentInterface 
{
	
	private static Hashtable<String,Hashtable<String,Double>> map;
	private static ArrayList<String> emoticonList;
	private final String[] basicSent = {"anger","disgust","fear","joy","sadness","surprise"};
	
	public SentimentInterface()
	{
		WordToSentimentMap initialize = new WordToSentimentMap();
    	map = initialize.getTermScores();
    	emoticonList = initialize.getEmoticonlist();
	}
	
    public double[] extractSentiments(String tweet)
    {
    	double[] score = new double[6];
    	int[] totalNum = new int[6];
    	for(String emoticon : emoticonList)
    	{
    		if(tweet.contains(emoticon))
    		{
    			int counter = 0;
    			for(String emotion : basicSent)
    			{
    				if(map.get(emoticon).containsKey(emotion))
    				{
    					if(emoticon.equals("o"))// Unfortunately there is an emoticon that is the same with letter "o", so it needs special treatment.
    					{
    						if(tweet.contains(" o "))
    						{
    							score[counter] +=  map.get(emoticon).get(emotion);
    	    					totalNum[counter] += 1;
    						}
    					}
    					else
    					{
    						score[counter] +=  map.get(emoticon).get(emotion);
        					totalNum[counter] += 1;
    					}
    				}
    				++counter;
    			}
    		}
    	}
    	tweet = tweet.toLowerCase();
    	Tokenizer tokenizer = new Tokenizer(tweet);
    	tokenizer.tokenize();
    	String[] words = tokenizer.getWords();
    	words = WordNetHandler.removeNumbers(words);
    	words = WordNetHandler.removeStopWords(words);
    	words = WordNetHandler.removeOneLetterWords(words);
    	words = WordNetHandler.removeLinks(words);
    	words = WordNetHandler.removeMentions(words);
    	for(String word : words)
    	{
    		String stems[] = WordNetHandler.getStems(word.trim());
    		if(stems.length > 0)
    		{
    			word = stems[0];
    			if(map.containsKey(word))
        		{
        			int counter = 0;
        			for(String emotion : basicSent)
        			{
        				if(map.get(word).containsKey(emotion))
        				{
        					score[counter] +=  map.get(word).get(emotion);
        					totalNum[counter] += 1;
        				}
        				++counter;
        			}
        		}
    		}
    	}
    	double totalSentimentalWords = 0;
    	for(int i=0;i<6;++i)
    	{
    		totalSentimentalWords += (double)totalNum[i];
    	}
    	double[] finalScores = new double[6];
    	for(int i=0;i<6;++i)
    	{
    		if(totalNum[i] != 0)
    		   finalScores[i] = (score[i]/totalNum[i])/totalSentimentalWords;
    		else
    			finalScores[i] = 0.0;
    	}
    	return finalScores;
    }

}
