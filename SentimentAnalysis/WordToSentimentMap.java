package SentimentAnalysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import javax.swing.JOptionPane;

import org.apache.commons.lang.ArrayUtils;

/*Class for reading basic words for each of the 6 sentiments, and expansion of the list through the use of 
 wordnet*/

public class WordToSentimentMap 
{
	private final static String basicSentimentsPath = "secondary_emotions.txt";
	private final static String emoticonsPath = "emoticons.txt";
	private final static String senticNetPath = "senticnet.rdf.xml";
	private static Hashtable<String,Hashtable<String,Double>> termScores;
	private static ArrayList<String> emoticonList;
	private static ArrayList<String> angerWords;
	private static ArrayList<String> disgustWords;
	private static ArrayList<String> fearWords;
	private static ArrayList<String> joyWords;
	private static ArrayList<String> sadnessWords;
	private static ArrayList<String> surpriseWords;
	
	public WordToSentimentMap()
	{
		emoticonList = new ArrayList<>();
		angerWords = new ArrayList<>();
		disgustWords = new ArrayList<>();
		fearWords = new ArrayList<>();
		joyWords = new ArrayList<>();
		sadnessWords = new ArrayList<>();
        surpriseWords = new ArrayList<>();
        addEmoticons();
        readBasicWords();
        parseSenticNet();
	}
	
	public void readBasicWords()
	{
		int loop = 0;
		@SuppressWarnings("unused")
		WordNetHandler wordnethandler = new WordNetHandler();
		LineSplitter linesplitter = new LineSplitter();
		//Each line of the arraylist contains basic words for each of the 6 basic sentiments.
		ArrayList<String> basicWords = linesplitter.splitToLines("", basicSentimentsPath);
		for(String line : basicWords)
		{
			++loop;
			String[] synonymSet = new String[0];
			Tokenizer tokenizer = new Tokenizer(line);
			tokenizer.tokenize();
			String[] words = tokenizer.getWords();
			for(String word : words)
			{
				String[] synonyms = WordNetHandler.getSynonyms(word);
				synonymSet = (String[])ArrayUtils.addAll(synonymSet, synonyms);
			}
			synonymSet= WordNetHandler.removeDuplicates(synonymSet);
			Arrays.sort(synonymSet);
			fillArraylists(loop,synonymSet);
		}
	}
	
	private static void addEmoticons()
	{
		termScores = new Hashtable<>();
		LineSplitter linesplitter = new LineSplitter();
		//Each line of the arraylist contains emoticons and their pos/neg score for each of the 6 basic sentiments.
		ArrayList<String> emoticons = linesplitter.splitToLines("", emoticonsPath);
		// for each line in the emoticons.txt file
		for(String line : emoticons)
		{
			//split the line to four parts based on tabs.
			String[] parts = line.split("\t");
			if(parts[0].trim().equals("shame"))
				parts[0] = "fear";
			if(!emoticonList.contains(parts[3].trim()))
                emoticonList.add(parts[3]);
			//scores[0] is the positive score, scores[1] is the negative score.
			double[] scores = new double[2];
			scores[0] = Double.parseDouble(parts[1].trim());
			scores[1] = Double.parseDouble(parts[2].trim());
			Hashtable<String,Double> innerTable = new Hashtable<>();
			if(scores[0] == 0.0)//if positive score is zero, add the negative score.
			    innerTable.put(parts[0].trim(),scores[1]);
			else//if negative score is zero, add the positive score.
				innerTable.put(parts[0].trim(),scores[0]);
			termScores.put(parts[3].trim(),innerTable);
		}
	}
	
	private static void fillArraylists(int i, String[] synset)
	{
		switch (i)
		{
		    case 1 : angerWords.addAll(Arrays.asList(synset));
		        break;
		    case 2 : disgustWords.addAll(Arrays.asList(synset));
		        break;
		    case 3 : fearWords.addAll(Arrays.asList(synset));
		        break;
		    case 4 : joyWords.addAll(Arrays.asList(synset));
		        break;
		    case 5 : sadnessWords.addAll(Arrays.asList(synset));
		        break;
		    case 6 : surpriseWords.addAll(Arrays.asList(synset));
		        break;
		    default : JOptionPane.showMessageDialog(null, "secondary_lines.txt has more than six lines.", "Warning",JOptionPane.WARNING_MESSAGE);
		        break;
		}   
	}
	
	public void parseSenticNet()
	{
		BufferedReader br;
		String line="";
		try 
		{
	        br = new BufferedReader(new FileReader(senticNetPath));
	        try 
	        {
	            while((line = br.readLine()) != null)
	            {
	            	line = line.trim();
                    if(line.startsWith("<text xmlns=\"http://sentic.net/api\">"))
                    {
                    	String[] parts = line.split("xmlns=\"http://sentic.net/api\">");
                    	String[] stems= WordNetHandler.getStems(parts[1].replace("</text>",""));
                    	if(stems.length > 0)
                    	{
                    		String concept = stems[0];
                        	ArrayList<String> secSentiments = getSecondarySentiment(concept);
                        	if(secSentiments.size() > 0)
                        	{
                        		for(int i = 0;i < 10 && line != null; ++i)
                            	{
                            		line = br.readLine();
                            	}
                            	parts = line.split("/XMLSchema#float\">");
                            	double polarity = Double.parseDouble(parts[1].replace("</polarity>",""));
                            	for(String sentiment : secSentiments)
                            	{
                            		Hashtable<String,Double> innerTable = new Hashtable<>();
                            		if(termScores.containsKey(concept))
                            		    innerTable = termScores.get(concept);
                            		innerTable.put(sentiment,Math.abs(polarity));
                            		termScores.put(concept, innerTable);
                            	}
                        	}
                    	}
                    }
	            }
	            angerWords.clear();
	            disgustWords.clear();
	            fearWords.clear();
	            joyWords.clear();
	            sadnessWords.clear();
	            surpriseWords.clear();
	            br.close();
	        } 
	        catch (IOException e) 
	        {
	            System.out.println("Error occured while opening SenticNet file");
	            e.printStackTrace();
	        }
	    } 
		catch (FileNotFoundException e) 
		{
            System.out.println("SenticNet file not found");
	        e.printStackTrace();
	    }
	}
	
	private ArrayList<String> getSecondarySentiment(String word)
	{
		ArrayList<String> result = new ArrayList<>();
		if(angerWords.contains(word))
			result.add("anger");
		if(disgustWords.contains(word))
			result.add("disgust");
		if(fearWords.contains(word))
			result.add("fear");
		if(joyWords.contains(word))
			result.add("joy");
		if(sadnessWords.contains(word))
			result.add("sadness");
		if(surpriseWords.contains(word))
			result.add("surprise");
		return result;
	}
	
	public Hashtable<String,Hashtable<String,Double>> getTermScores()
	{
		return termScores;
	}
	
	public ArrayList<String> getEmoticonlist()
	{
		return emoticonList;
	}
	
}
