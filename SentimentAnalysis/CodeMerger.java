package SentimentAnalysis;

import java.util.ArrayList;
import java.util.Iterator;
import twitterAPI.MongoHandler;
import twitterAPI.MongoHandler.TweetObject;

public class CodeMerger 
{
    public static void extractSentimentsAndEliminateDuplicates()
    {	
        MongoHandler mongo = new MongoHandler();
        mongo.fetchUsersAndTrends();// Fetch usernames.
		
		Iterator<String> users = mongo.getUsers().iterator();// Create an iterator on all usernames.
		
		SentimentInterface sentimentExtractor = new SentimentInterface();// Call constructor for sentiment extractor object.
		
		while(users.hasNext())// while there are more users
		{
			String u = users.next();// u is the name of the current user.
			
			//System.out.println("-------------------------------------------------- User: "+u+" -------------------------------");
			
			ArrayList<TweetObject> tweets = mongo.getTweets(u);// arraylist of all tweets for the current user.
			
			//Iterator<String> it = tweets.iterator();// iterator on all the tweets of the current user.
			
			int currentTweetId;// id of the current tweet (not the real id, a fake one to help the calculations).
			
			for(currentTweetId=0;currentTweetId<tweets.size();++currentTweetId)// while the tweetlist has more tweets
			{
				String text = tweets.get(currentTweetId).text();// text contains the body of the tweet.
				//System.out.println("Current tweet "+text);
				//System.out.println();
				double[] sentiments = sentimentExtractor.extractSentiments(text);// extract sentiments of the tweet.
				
				// TODO save sentiments to mongo database here.
				
				for(int i=currentTweetId+1;i<tweets.size();++i)// for every other tweet of the same user except the ones that come before it
				{
					int distance = LevensteinDistance.getLevensteinDistance(text, tweets.get(currentTweetId).text());
					//System.out.println();
					//System.out.println(distance);
					//System.out.println();
					if(distance < 10)
					{
						//System.out.println("Duplicate tweet of user "+u+" with text "+tweets.get(i)+" deleted.");
						//System.out.println();
						//TODO delete tweet from mongo database here.
						
						tweets.remove(i);// remove the duplicate tweet from the arraylist.
						--i;// We reduce the counter by one because the arraylist positions shift to the left by one after removing a tweet.
					}
				}
			}
			//System.out.println("-----------------------------------------------------------------------------------");
		}
    	
    }
}
