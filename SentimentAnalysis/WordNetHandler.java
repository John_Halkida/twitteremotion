package SentimentAnalysis;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;

import rita.RiWordNet;
import rita.support.PorterStemmer;

public class WordNetHandler 
{
	
	private static RiWordNet wordnet;
	
	public WordNetHandler()
	{
		wordnet = new RiWordNet("WordNet-3.0");
	}
	
	public static String[] getSynonyms(String word)
	{
		String[] synset = new String[0];
		String[] stems = getStems(word);// An array that contains all stems of the given word.
		String[] pos = new String[0];
		//Add the first stems to the synset array.
		synset = (String[])ArrayUtils.addAll(synset,stems);
		//For every stem of the word
		for(int i=0;i<stems.length;++i)
		{
			pos = wordnet.getPos(stems[i]);
			//For every part of speech the stem is a part of, get a synset and concatenate it to the rest of the synsets.
			for(int j=0;j<pos.length;++j)
			{
				synset = (String[])ArrayUtils.addAll(synset,wordnet.getSynonyms(stems[i], pos[j]));
			}
		}
		//remove duplicates from the stemmed array
		synset = getSynsetStems(synset);
		synset = removeDuplicates(synset);
		return synset;
	}
	
	public static String[] removeDuplicates(String[] arr)
	{
        Set<String> alreadyPresent = new HashSet<String>();
	    String[] whitelist = new String[0];
	    for (String nextElem : arr) 
	    {
           if (!alreadyPresent.contains(nextElem.trim()))
           {
	          whitelist = Arrays.copyOf(whitelist, whitelist.length + 1);
	          whitelist[whitelist.length - 1] = nextElem.trim();
		      alreadyPresent.add(nextElem.trim());
		   }
	    }
	    return whitelist;
	}
	
	public static String[] getStems(String word)
	{
		//Find all noun stems, all verb stems, all adjective stems etc.
		String[] nounStems = wordnet.getStems(word,"n");
		String[] verbStems = wordnet.getStems(word,"v");
		String[] adjStems = wordnet.getStems(word,"a");
		String[] advStems = wordnet.getStems(word,"r");
		String[] stems = (String[])ArrayUtils.addAll(nounStems, verbStems);
		stems = (String[])ArrayUtils.addAll(stems, adjStems);
		stems = (String[])ArrayUtils.addAll(stems, advStems);
		stems = removeDuplicates(stems);
		return stems;
	}
	
	private static String[] getSynsetStems(String[] synset)
	{
        ArrayList<String> stemmedSynset = new ArrayList<>();
        for(String word : synset)
        {
        	String[] stems = getStems(word);
        	if(stems.length > 0)
        	    stemmedSynset.add(stems[0]);
        }
        String[] newSynset = stemmedSynset.toArray(new String[stemmedSynset.size()]);
        return newSynset;
	}
	
	public static String[] porterStemmer(String[] words)// Maybe won't be needed
	{
		ArrayList<String> stemmedWords = new ArrayList<>();
		PorterStemmer porterstemmer = new PorterStemmer();
		for(String word : words)
		{
			stemmedWords.add(porterstemmer.stem(word));
		}
		return stemmedWords.toArray(new String[stemmedWords.size()]);
	}
	
	 public static String[] removeOneLetterWords(String[] arr)
	 {
	   	String[] whitelist = new String[0];
	    for (String nextElem : arr)
	    {
	    	if(nextElem.length() > 1)
	    	{
	    		whitelist = Arrays.copyOf(whitelist, whitelist.length + 1);
	  	        whitelist[whitelist.length - 1] = nextElem;
	    	}
	    }
	    return whitelist;
     }
	 
	public static String[] removeStopWords(String[] arr)
	{
	    String[] whitelist = new String[0];
		for (String nextElem : arr) 
	    {
	       if (!StopWords.isStopword(nextElem))
	       {
	          whitelist = Arrays.copyOf(whitelist, whitelist.length + 1);
	          whitelist[whitelist.length - 1] = nextElem;
		   }
	    }
		return whitelist;
	}
	
	public static String[] removeNumbers(String[] words)
    {
    	ArrayList<String> newWords = new ArrayList<>();
    	for(String word : words)
    	{
    		if(!word.trim().matches(".*\\d+.*"))
    			newWords.add(word);
    	}
    	return newWords.toArray(new String[newWords.size()]);
    }
	
	public static String[] removeLinks(String[] words)
    {
    	ArrayList<String> newWords = new ArrayList<>();
    	for(String word : words)
    	{
    		try 
    		{
    		    @SuppressWarnings("unused")
				URL url = new URL(word);
    		} 
    		catch (MalformedURLException e) // If it isn't a URL
    		{
    	        newWords.add(word);
    		}
    	}
    	return newWords.toArray(new String[newWords.size()]);
    }
	
	public static String[] removeMentions(String[] words)
    {
    	ArrayList<String> newWords = new ArrayList<>();
    	for(String word : words)
    	{
    		if(!word.trim().startsWith("@"))
    			newWords.add(word);
    	}
    	return newWords.toArray(new String[newWords.size()]);
    }
	
}
